output "password" {
  description = "display generated password"
  value       = random_password.pass.result
  sensitive   = true
}
